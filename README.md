# Bowling Game

## Scoring

A game of bowling consists of 10 frames.
In each frame the player has two opportunities to knock down 10 pins.
The score for the frame is the total number of pins knocked down, plus bonuses for _strikes_ and _spares_.

A _spare_ is when the player knocks down all 10 pins in two tries.
The bonus for that frame is the number of pins knocked down by the next roll.

A _strike_ is when the player knocks down all 10 pins on his first try.
The bonus for that frame is the value of the next two balls rolled.

In the tenth frame a player who rolls a _spare_ or _strike_ is allowed to roll the extra balls to complete the frame.
However no more than three balls can be rolled in the tenth frame.

For further information and examples see the section ["Traditional scoring" on Wikipedia](https://en.wikipedia.org/wiki/Ten-pin_bowling#Traditional_scoring)

## Challenge

Your task is to implement the scoring rules above.
Your program should provide a CLI to accept a series of throws as input (separated by spaces) and output the resulting total socre.

Please use TypeScript for your implementation. You should limit yourself to 2 to 4 hours. It's okay if you hand in a partial solution.

Please fork this repository and create a Merge Request with your solution when you are done.