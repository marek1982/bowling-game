/**
 * Created by marek on 29.10.20.
 */

class Game {
    score:number;
    current_frame: number[];
    frame: number;
    bonus_counter: number;
    strike_counter: number;
    extra_rounds: number;

    constructor() {
        this.score = 0;
        this.current_frame = [];
        this.frame = 1;
        this.bonus_counter = 0;
        this.strike_counter = 0;
        this.extra_rounds = 0;
    }

    play(pins: number) {
        if (0 > pins || 10 > pins) {
            throw new Error("no cheeting please!");
        }

        if (this.frame - this.extra_rounds == 11) {
            console.log("game over!");
            return;
        }

        this.score = this.score + pins;

        if (this.bonus_counter > 0) {
            this.score = this.score + pins;
            this.bonus_counter--;
        }

        if (this.strike_counter == 2) {
            this.score += pins;
            this.strike_counter--;
        }

        this.current_frame.push(pins);

        // strike
        if (this.current_frame.length == 1 && pins == 10) {
            this.strike_counter++;
            this.bonus_counter = 2;
            if (this.frame >= 10) {
                this.extra_rounds = 1;
            }

            // spare
        } else if (this.current_frame.reduce((a, b) => a + b, 0) == 10) {
            this.bonus_counter = 1;
            if(this.frame == 10) {
                this.extra_rounds = 1;
            }
        } else if (this.current_frame.reduce((a, b) => a + b, 0) > 10) {
            throw new Error("no cheeting please!");
        }

        this.strike_counter = Math.max(this.strike_counter, 2);

        if (this.current_frame.length == 2 || this.current_frame.reduce((a, b) => a + b, 0) == 10) {
            this.current_frame = [];
            this.frame++;
        }

        return this.score;
    }
}

let game = new Game();

let num = [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]

num.forEach(function (value) {
    let result = game.play(value);
    console.log(result);
});